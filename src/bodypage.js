import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import './navbar.css';
import './content.css';
import Appbar from 'muicss/lib/react/appbar';
import Button from 'muicss/lib/react/button';
import Container from 'muicss/lib/react/container';
import Panel from 'muicss/lib/react/panel';
import Row from 'muicss/lib/react/row';
import Col from 'muicss/lib/react/col';
import Content1 from './content1';
import Menubar from './menubar';
import { ResponsiveContainer , PieChart, Pie, Sector, Cell ,Tooltip } from 'recharts';

const data = [{name: 'Group A', value: 400}, {name: 'Group B', value: 300},
                  {name: 'Group C', value: 300}, {name: 'Group D', value: 200}];
const COLORS = ['#0088FE', '#00C49F', '#FFBB28', '#FF8042'];

const RADIAN = Math.PI / 180;

class Bodypage extends Component {
  render() {
    return (
      <div>
      <Container fluid={true} style={{'margin-top':'70px' ,'float': 'right',
    'width': 'calc(100% - 260px)'}}>
      <Row>
        <Col md="4">
            <Panel className="animated zoomInUp" >
            <ResponsiveContainer>
            <PieChart onMouseEnter={this.onPieEnter}>
            <Tooltip />
                    <Pie
                      data={data}
                      innerRadius={60}
                      outerRadius={80}
                      fill="#8884d8"
                      paddingAngle={5}
                      label

                    >
                    	{
                      	data.map((entry, index) => <Cell fill={COLORS[index % COLORS.length]}/>)
                      }
                      <Tooltip/>
                    </Pie>
                    </PieChart>
              </ResponsiveContainer>
            </Panel></Col>
        <Col md="4">
            <Panel className="animated zoomInUp" >

            </Panel></Col>
        <Col md="4">
            <Panel className="animated zoomInUp" >

            </Panel></Col>

      </Row>
      <Row>
      {this.props.handleGraph === 1 ?
            <Content1 />: null
      }
      {this.props.handleGraph === 2 ?
            <h1>Doc1</h1>: null
      }
      {this.props.handleGraph === 3 ?
            <h1>Doc2</h1>: null
      }
      </Row>
      </Container>
      </div>
    );
  }
}

export default Bodypage;

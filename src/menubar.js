import React, { Component } from 'react';
import logo from './logo.svg';
import '../node_modules/muicss/dist/css/mui.min.css';
import './App.css';
import './menubar.css';
import './navbar.css';
import Appbar from 'muicss/lib/react/appbar';
import Button from 'muicss/lib/react/button';
import Container from 'muicss/lib/react/container';
import Input from 'muicss/lib/react/input';
import Divider from 'muicss/lib/react/divider';
import './flareg.ico';



class Menubar extends Component {
  constructor(props){
    super(props)
    this.state = {
      select : 0,              // เซ็ตให้ตัวแปร select เป็นตัวระบุกราฟ
    }
  }

  render() {
    return (
          <div className='menu-bar animated pulse'>
          <div><h1 style={{'text-align':'center'}}><span><img src='flareg.ico' alt='Smiley face' /></span>FlareViz</h1></div>
              <Divider />
                <ul>
                  <li onClick={()=> this.props.handleChangeState(1)} >Graph-1</li>  {/* เปลี่ยนกราฟ ปุ่ม 1 */}
                  <li onClick={()=> this.props.handleChangeState(2)} >Graph-2</li>
                  <li onClick={()=> this.props.handleChangeState(3)} >Graph-3</li>
                </ul>
          </div>
  );
  }
}

export default Menubar;

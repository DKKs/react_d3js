import React, { Component } from 'react';
import logo from './logo.svg';
import '../node_modules/muicss/dist/css/mui.min.css';
import './App.css';
import './navbar.css';
import Appbar from 'muicss/lib/react/appbar';
import Button from 'muicss/lib/react/button';
import Container from 'muicss/lib/react/container';
import Input from 'muicss/lib/react/input';
import Row from 'muicss/lib/react/row';
import Col from 'muicss/lib/react/col';
import Option from 'muicss/lib/react/option';
import Select from 'muicss/lib/react/select';
import Divider from 'muicss/lib/react/divider';

const btstyle = {
  color: '#333',
  height: '50px',
  fontSize: '20px',
  backgroundColor: '#EEEEEE',
  marginTop: '10px',
};

class Navbar extends Component {
  render() {
    return (
      <div className='navbar'>
        <Container fluid={true} style={{'padding':'2px'}}>
          <Row>
            <Col md="3"> <Button variant="flat" style={btstyle}>OverViews</Button></Col>
            <Col md="3"></Col>
            <Col md="3"></Col>
            <Col md="3"></Col>
          </Row>
        </Container>
        <Divider />
      </div>
    );
  }
}

export default Navbar;

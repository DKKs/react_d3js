import React, { Component } from 'react';
import logo from './logo.svg';
import '../node_modules/muicss/dist/css/mui.min.css';
import './menubar.css';
import './App.css';
import './navbar.css';
import Navbar from './navbar';
import Appbar from 'muicss/lib/react/appbar';
import Menubar from './menubar';
import Bodypage from './bodypage';



class App extends Component {
  constructor(props){
    super(props)
    this.state = {
      select : 0,
    }
  }
  handleChangeState = (input)=>{
      this.setState({select : input})
  }
  render() {
    return (
      <div className="App">
        <Menubar handleChangeState = {this.handleChangeState}

        />
        <Navbar />
        <Bodypage handleGraph = {this.state.select}/>
      </div>
    );
  }
}

export default App;
